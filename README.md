# Broken -- Mega Challenge Part 2

## Challenge description
Sometimes things that are broken have been broken on purpose.

## Required files for hosting
`broken.img`

## Hosting instructions
- The included file should be distributed through the challenge Cup of Joe.
- This challenge is worth 100 points and should require the challenge Cup of Joe to be solved before it's visible.
- The challenge should include the following md5sum of itself to confirm Cup of Joe correctly sent the required file:
- md5sum: b254335b222ae6a11639fd1625e567a1
- Note: when fixed, this distributes the pwnable binary needed for the Rop Me Like A Hurricane challenge.

## Hints
- What is this file?
- How might you fix this file?

## Flag 
`TUCTF{D1S4ST3R_R3C0V3RY}`