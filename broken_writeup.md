# Broken -- TUCTF 2019

The file you get after solving Cup of Joe is titled broken.img -- running file on this tells you it's an ext4 filesystem, but trying to mount it results in an error.
The disk image was corrupted on purpose via 'sudo dd if=/dev/zero of=/dev/sdb1 bs=1k seek=10 count=4k'
Just by running `e2fsck -y broken.img`, the image will be repaired and is mountable again. 
When mounted, this challenge's flag and the next challenge's required binary is found in /<mountpoint>/lost+found/#65281#/sbin/, alongside other random Ubuntu stock files. 
